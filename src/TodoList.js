import React from 'react'

class TodoList extends React.Component{
    
    
    render(){
        
        let completedStyle = {textDecoration: "line-through"}
        
        let taskArray = []
        
        for(let i = 0; i < this.props.tasks.length; i++ ){
            taskArray.push(<li  key={this.props.tasks[i].key} 
                                onClick={() => {this.props.taskComplete(this.props.tasks[i].key)}} 
                                style={this.props.tasks[i].completed ? completedStyle : null} >
                                {this.props.tasks[i].task}
                                
                            </li>)
        }

        let displaytaskArray = this.props.tasks.length > 0  ? taskArray : <div>Escriba las tareas a realizar</div>
        
        return (
            <div>
                {displaytaskArray} 
            </div>
        ) 
    }
}

export default TodoList