import React from 'react'
import TodoList from './TodoList'

class App extends React.Component  {
  constructor(){
    super()
    this.countKey = 1
  }

  state = {
    todos: [],
    newTodo: ""
  }

  inputChange = (event) => {
    this.setState({
      newTodo: event.target.value
    })  
    
  }
  
  handleButton = (event) => {
    event.preventDefault()
    
    let item = [{ task: this.state.newTodo, 
                  key: this.countKey, 
                  completed: false
                }]
    this.countKey++

    this.setState({
      todos: this.state.todos.concat(item),
      newTodo: ""
    })

    document.getElementById("clear-form").reset()  
  }

  vaciarButton = (event) => {
    event.preventDefault()
    this.setState({
      todos: []
    })
  }

  taskComplete = (id) => {
    const todos = this.state.todos.map(todo => {
      if(todo.key === id){
        return {
          task: todo.task,
          key: todo.key,
          completed: !todo.completed
        }
      }
      return todo
      
    })
    this.setState({
      todos: todos
    })
  }

  render(){
    
    return (
      <div>
        <ul>
          <TodoList  tasks={this.state.todos} taskComplete={this.taskComplete}/>
        </ul>
        <div>
          <form id="clear-form">
            <input type="text" onChange={this.inputChange}/>
            <button onClick={this.handleButton}>Enviar</button>   
            <button onClick={this.vaciarButton}>Vaciar la lista</button> 
          </form>
        </div>
      </div>
    );
  }
}

export default App;
